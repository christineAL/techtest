## READ ME ##
This project uses vagrant to create N virtual machines and configure loadbalancer on another virtual machine. Ansible is used for Provisioning the virtual machines.

* Run vagrant up
* Run vagrant provision
